﻿using BDDSample.Persistence;
using BDDSample.Services.Handlers.Persons;
using BDDSample.Services.Handlers.Persons.Contracts;
using BDDSample.Services.Handlers.Persons.Contracts.Dtos;
using BDDSample.Tools.Persons.Roles;
using BDDSample.Tools.Persons.Users;

namespace BDDSample.Tools.Handlers.Presons
{
    public static class AddUserHandlerFactory
    {
        public static UserHandler CreateHandler(EFDataContext context)
        {
            var userService = UserFactory.CreateService(context);
            var roleService = RoleFactory.CreateService(context);
            var unitOfWork = new DummyUnitOfWork(context);
            return new AddUserCommandHandler(unitOfWork, userService, roleService);
        }

        public static AddUserCommand CreateCommand(string phoneNumber,string roleName)
        {
            return new AddUserCommand
            {
                PhoneNumber = phoneNumber,
                RoleName = roleName
            };

        }
    }
}
