﻿using BDDSample.Test.Spec.Infrastructure;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;
using BDDSample.Tools.Handlers.Presons;

namespace BDDSample.Test.Spec.Persons.Users
{
    [Story("کاربران",
        AsA = "مدیر ",
        IWantTo = "کارمندانم را تعریف نمایم "
        , InOrderTo = "کارمندانم را مدیریت کنم")]

    [Scenario("ثبت کابر جدید")]
    public class RegisterUser : SpecTestPersistence
    {
        public RegisterUser(ConfigurationFixture configuration) : base(configuration)
        {
        }

        [Given("فهرست کاربران به صورت خالی وجود دارد.")]
        private void Given()
        {
        }

        [When("فردی با شماره تماس ‘09035331828’ را ثبت میکنم.")]
        private async Task When()
        {
            var command = AddUserHandlerFactory.CreateCommand("09035331828", "user");
            var sut = AddUserHandlerFactory.CreateHandler(Context);

            await sut.Command(command);

        }

        [Then("باید تنها یک فرد با شماره تماس ‘09035331828’ در فهرست کاربران  با نقش ‘user’ وجود داشته باشد.")]
        private async Task Then()
        {
            var expected= await ReadDataContext.Users.Include(_=>_.Role).SingleAsync();
            expected.PhoneNumber.Should().Be("09035331828");
            expected.Role!.Name.Should().Be("user");
        }

        [Fact]
        public async Task Run()
        {
            Given();
            await When();   
            await Then();   
        }
    }
}
