﻿using BDDSample.Persistence;
using BDDSample.Persistence.Persons.Roles;
using BDDSample.Services.Persons.Roles;
using BDDSample.Services.Persons.Roles.Contracts;
using BDDSample.Services.Persons.Roles.Contracts.Dtos;
using BDDSample.Test.Unit.Infrastructure;
using BDDSample.Tools.Persons.Roles;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BDDSample.Test.Unit.Persons.Roles
{
    public class RoleServiceTest : UnitTestPersistence
    {
        private readonly IRoleService _sut;
        public RoleServiceTest()
        {
            _sut = RoleFactory.CreateService(Context);
        }

        [Fact]
        private async Task Add_create_role()
        {
            var dto = new AddRoleDto
            {
                Name = "dummy_name"
            };

            await _sut.Add(dto);

            var expected = await ReadDataContext.Roles.SingleAsync();
            expected.Name.Should().Be(dto.Name);
        }
    }
}
