﻿using BDDSample.Test.Unit.Infrastructure;
using BDDSample.Tools.Handlers.Presons;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BDDSample.Test.Unit.Handlers.Persons
{
    public class AddUserCommandHandlerTests : UnitTestPersistence
    {
        public AddUserCommandHandlerTests() { }

        [Fact]
        private async Task Command_add_user_by_role()
        {
            var command = AddUserHandlerFactory.CreateCommand("dummy_phone", "dummy_role_name");
            var sut = AddUserHandlerFactory.CreateHandler(Context);
            await sut.Command(command);

            var expected = await ReadDataContext.Users.Include(_=>_.Role).SingleAsync();
            expected.PhoneNumber.Should().Be(command.PhoneNumber);
            expected.Role!.Name.Should().Be(command.RoleName);    
        }
    }
}
