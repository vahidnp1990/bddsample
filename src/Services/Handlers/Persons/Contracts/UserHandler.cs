﻿using BDDSample.Services.Handlers.Persons.Contracts.Dtos;

namespace BDDSample.Services.Handlers.Persons.Contracts
{
    public interface UserHandler
    {
        Task Command(AddUserCommand command);
    }
}
