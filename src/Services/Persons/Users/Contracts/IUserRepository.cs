﻿using BDDSample.Entities.Persons;

namespace BDDSample.Services.Persons.Users.Contracts
{
    public interface IUserRepository
    {
        void Add(User user);
    }
}
