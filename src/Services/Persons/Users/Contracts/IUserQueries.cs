﻿using BDDSample.Services.Persons.Users.Contracts.Queries;

namespace BDDSample.Services.Persons.Users.Contracts
{
    public interface IUserQueries
    {
        Task<GetUserDto?> GetUserById(long Id);
    }
}
