﻿using BDDSample.Entities.Persons;

namespace BDDSample.Services.Persons.Roles.Contracts
{
    public interface IRoleRepository
    {
        void Add(Role role);
        Task<Role?> FindByName(string roleName);
    }
}
