﻿namespace BDDSample.Services.Infrastructure.Data
{
    public interface IUnitOfWork
    {
        Task Begin();
        Task Commit();
        Task CommitPartial();
        Task Complete();
        Task Rollback();
    }
}
