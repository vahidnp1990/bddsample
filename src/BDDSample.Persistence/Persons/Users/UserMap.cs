﻿using BDDSample.Entities.Persons;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BDDSample.Persistence.Persons.Users
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> _)
        {
            _.ToTable("Users");
            _.HasKey(x => x.Id);
            _.Property(_ => _.PhoneNumber).HasMaxLength(20);
        }
    }
}
