﻿using BDDSample.Entities.Persons;
using BDDSample.Services.Persons.Users.Contracts;
using Microsoft.EntityFrameworkCore;

namespace BDDSample.Persistence.Persons.Users
{
    public class UserRepository : IUserRepository
    {
        private readonly DbSet<User> _users;
        public UserRepository(EFDataContext context)
        {
            _users = context.Users;
        }
        public void Add(User user)
        {
            _users.Add(user);   
        }
    }
}
